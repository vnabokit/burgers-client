# Frontend of training project "Burgers"

The training project "Burgers" demonstrates simple usage of CRUD operations.
It is assumed this frontend in used in coupling with the respective backend. 

## Stack

* React.JS
* Redux
* Redux Final Form
* Axios
* Bootstrap

# App demo

The app demo (frontend + backend) is available:  <https://burgers-client.herokuapp.com>

# Backend source code

<https://gitlab.com/vnabokit/burgers-server.git>


# How to deploy

1. Download this repo
2. Edit ```REACT_APP_SERVER_BASE_URL``` variable into the ```.env``` file
3. Deploy and start the server (see README.md of the backend part)
4. Launch ```npm install```
5. Launch ```npm start```  
