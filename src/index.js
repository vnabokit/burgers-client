import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import store from './store/store';
import AppW from './components/wrapComponents/App_w';

ReactDOM.render(
  <Provider store={store}>
    <AppW />
  </Provider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
