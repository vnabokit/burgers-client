import { bindActionCreators } from 'redux';
import { itemAdded, itemUpdated, itemDeleted, itemsFetched, dbConnected } from './actionCreators/actions';

function mapDispatchToProps(component) {
	switch (component) {
		case "ItemAdd": return function (dispatch) {
			return {
				changeItemAdded: bindActionCreators(itemAdded, dispatch)
			};
		};
		case "ItemsList": return function (dispatch) {
			return {
				changeItemAdded: bindActionCreators(itemAdded, dispatch),
				changeItemDeleted: bindActionCreators(itemDeleted, dispatch),
				changeItemsFetched: bindActionCreators(itemsFetched, dispatch),
				changeDbConnected: bindActionCreators(dbConnected, dispatch)
			};
		};
		case "ItemView": return function (dispatch) {
			return {
				changeItemUpdated: bindActionCreators(itemUpdated, dispatch),
				changeItemDeleted: bindActionCreators(itemDeleted, dispatch),
			};
		};
		default: return undefined;
	}
}

export default mapDispatchToProps;