function mapStateToProps(component) {
	switch (component) {
		case "ItemAdd": {
			return function (state) {
				return {
					itemAdded: state.itemAdded,
					isDbConnected_Redux: state.isDbConnected
				};
			}
		}
		case "ItemsList": {
			return function (state) {
				return {
					items: state.items,
					isDbConnected_Redux: state.isDbConnected
				};
			}
		}
		case "ItemView": {
			return function (state) {
				return {
					itemUpdated: state.itemUpdated,
					itemDeleted: state.itemDeleted
				};
			}
		}
		default: return undefined;
	}
}

export default mapStateToProps;