const initialState = {
  items: [],
  itemAdded: {},
  itemUpdated: {},
  itemDeleted: {},
  isDbConnected: true
};

export default initialState;
