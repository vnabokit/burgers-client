export function itemAdded( item ) {
	return {
		type: 'items/itemAdded',
		payload: item
	};
}

export function itemUpdated( item ) {
	return {
		type: 'items/itemUpdated',
		payload: item 
	};
}

export function itemDeleted( id ) {
	return {
		type: 'items/itemDeleted',
		payload: id
	};
}

export function itemsFetched( items ) {
	return {
		type: 'items/fetched',
		payload: items
	};
}

export function dbConnected( isConnected ) {
	return {
		type: 'service/dbConnected',
		payload: isConnected
	};
}
