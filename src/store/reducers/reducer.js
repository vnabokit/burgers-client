import initialState from '../initialState';

function reducer( state = initialState , action ){
  switch( action.type ){
    case 'items/itemAdded' : {
      return {
        ...state,
        itemAdded: action.payload,
        items: state.items.concat( [action.payload] )
      }
    }
    case 'items/itemDeleted' : {
      return {
        ...state,
        items: state.items.filter( item => { return item.id !== action.payload.id; } ),
        itemDeleted: action.payload
      }
   }
    case 'items/itemUpdated' : {
      return {
        ...state,
        itemUpdated: action.payload
      }
    }
    case 'items/fetched' : {
      return {
        ...state,
        items: action.payload
      }
    }
    case 'service/dbConnected' : {
      return {
        ...state,
        isDbConnected: action.payload
      }
    }
    default :
      return state;
  }

}

export default reducer;

