/**
 * Treat user entered item data before sending it to a server
 * @param {object} item0 - an item object
 * @return {object}
 * @example
 * let item0 = { title: "My title ", price: "aaa", description: "My descr" };
 * item1 = cleanData( item0 );
 * item1 == { title: "My title", price: 0, description: "My descr", imgSrc: "" }
 */
export const cleanData = (item0) => {
  let item = { ...item0 };
  if (typeof item.title == 'undefined') item.title = '';
  item.title = item.title.trim();
  if (typeof item.price == 'undefined') item.price = 0;
  let priceCleaned = parseFloat(item.price);
  item.price = isNaN(priceCleaned) ? 0 : parseFloat(priceCleaned.toFixed(2));
  if (typeof item.description == 'undefined') item.description = '';
  item.description = item.description.trim();
  if (typeof item.imgSrc == 'undefined') item.imgSrc = '';
  return item;
};
