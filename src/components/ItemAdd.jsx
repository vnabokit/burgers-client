import React, { Component } from 'react';
import axios from 'axios';
import { Form, Field } from 'react-final-form';
import ReactModal from 'react-modal';
import { cleanData } from '../common';

const customStyles = {
  content: {
    top: '35%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

const validateTitle = async (value) => {
  let error = undefined;
  if (typeof value == 'undefined') error = 'Required';
  else if (typeof value != 'undefined') {
    const res = await axios.get( process.env.REACT_APP_SERVER_BASE_URL + '/items/titles' );
    res.data.map((obj) => {
      if (obj.title.toLowerCase() === value.trim().toLowerCase()) {
        error = 'Such title is already in use!';
      }
      return obj.title;
    });
  }
  return error;
};

class ItemAdd extends Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
    };
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  onSubmit = (values) => {
    let itemCleaned = cleanData( values );
    const item = {
      title: itemCleaned.title,
      price: itemCleaned.price,
      description: itemCleaned.description,
      imgSrc: itemCleaned.imgSrc,
    };
    let changeItemAdded = this.props.changeItemAdded;
    axios
      .post( process.env.REACT_APP_SERVER_BASE_URL + '/items/create', {
        item,
      })
      .then(function (response) {
        changeItemAdded( { id: response.data.id, title: item.title } );
      })
      .catch(function (error) {
        console.log(error);
      });
    this.handleCloseModal();
  };

  showAddingForm() {
    const styleControls = {
      width: "250px"
    };
    return (
      <div>
        <h4>Add new burger</h4>
        <Form
          onSubmit={this.onSubmit}
          render={({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <div>
                <Field name="title" validate={validateTitle}>
                  {({ input, meta }) => (
                    <div>
                      <div style={{ height: '10px' }}></div>
                      <input {...input} type="text" placeholder="Title" style={styleControls}/>
                      <br />
                      <div style={{ height: '30px' }}>
                        {meta.error && meta.touched && (
                          <span style={{ color: 'red' }}>{meta.error}</span>
                        )}
                      </div>
                    </div>
                  )}
                </Field>
                <Field
                  name="price"
                  component="input"
                  type="text"
                  className="mb-2"
                  style={styleControls}
                  placeholder="Price"
                  validateFields={[]}
                />
                <br />
                <Field
                  name="description"
                  component="textarea"
                  className="mb-2"
                  style={styleControls}
                  placeholder="Description"
                  validateFields={[]}
                />
                <br />
                <Field
                  name="imgSrc"
                  component="input"
                  type="text"
                  className="mb-2"
                  style={styleControls}
                  placeholder="Image URL"
                  validateFields={[]}
                />
                <br />
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                style={{ margin: '3px' }}
              >
                Add
              </button>
              <button
                type="button"
                className="btn btn-secondary"
                style={{ margin: '3px' }}
                onClick={this.handleCloseModal}
              >
                Cancel
              </button>
            </form>
          )}
        />
      </div>
    );
  }

  render() {
    return (
      <div className="text-end" style={{ margin: '10px' }}>
        <button
          type="button"
          className="btn btn-link"
          onClick={this.handleOpenModal}
          disabled={this.props.isDbConnected_Redux?false:true}
        >
          Add burger
        </button>
        <ReactModal
          isOpen={this.state.showModal}
          contentLabel="Add burger"
          ariaHideApp={false}
          style={customStyles}
        >
          {this.showAddingForm()}
        </ReactModal>
      </div>
    );
  }
}

export default ItemAdd;
