import { connect } from 'react-redux';
import ItemsList from '../ItemsList';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const ItemsListW = connect(mapStateToProps("ItemsList"), mapDispatchToProps("ItemsList"))(ItemsList);

export default ItemsListW;