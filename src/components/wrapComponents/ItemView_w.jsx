import { connect } from 'react-redux';
import ItemView from '../ItemView';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const ItemViewW = connect(mapStateToProps("ItemView"), mapDispatchToProps("ItemView"))(ItemView);

export default ItemViewW;