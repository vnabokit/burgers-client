import { connect } from 'react-redux';
import App from '../../App';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const AppW = connect(mapStateToProps("App"), mapDispatchToProps("App"))(App);

export default AppW;