import { connect } from 'react-redux';
import ItemAdd from '../ItemAdd';
import mapStateToProps from '../../store/mapStateToProps';
import mapDispatchToProps from '../../store/mapDispatchToProps';

const ItemAddW = connect(mapStateToProps("ItemAdd"), mapDispatchToProps("ItemAdd"))(ItemAdd);

export default ItemAddW;