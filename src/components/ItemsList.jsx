import React, { Component } from 'react';
import axios from 'axios';
import ItemViewW from './wrapComponents/ItemView_w';

class ItemsList extends Component {
  constructor() {
    super();
    this.fetchItems();
  }

  state = {
    items: [],
    isDbConnected: true,
  };

  setItems(items) {
    this.setState({ items });
  }

  fetchItems = async () => {
     await axios
      .get( process.env.REACT_APP_SERVER_BASE_URL + '/items')
      .then((res) => {
        this.setState({ isDbConnected: true });
        this.props.changeDbConnected(true); //for Redux
        this.setItems(res.data);
        this.props.changeItemsFetched(
          res.data.map((item) => {
            return { id: item.id, title: item.title };
          }),
        );
      })
      .catch((err) => {
        console.log(err);
        this.setState({ isDbConnected: false });
        this.props.changeDbConnected(false); //for Redux
      });
  };

  componentDidUpdate(prevProps) {
    const isInitialState =
      typeof prevProps.items.length === 'undefined' ||
      prevProps.items.length === 0;
    if (this.props.items.length !== prevProps.items.length && !isInitialState)
      this.fetchItems();
  }

  render() {
    return this.state.isDbConnected ? (
      <div className="row row-cols-1 row-cols-md-4 g-4">
        {this.state.items.map((item) => (
          <ItemViewW item={item} key={item.id} onDelete={this.handleDelete} />
        ))}
      </div>
    ) : (
      <div>
        Issue with DB connection. Check <b>.env</b> configuration file on the web-server.
      </div>
    );
  }

  handleDelete = (itemId, itemTitle) => {
    console.log(itemId);
    let changeItemDeleted = this.props.changeItemDeleted;
    axios
      .delete( process.env.REACT_APP_SERVER_BASE_URL + '/items/delete', {
        data: {
          id: itemId,
        },
      })
      .then(function (response) {
        changeItemDeleted({ id: itemId, title: itemTitle });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
}

export default ItemsList;
