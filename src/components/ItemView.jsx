import React, { Component } from 'react';
import axios from 'axios';
import ReactHtmlParser from 'react-html-parser';
import { cleanData } from '../common';

class ItemView extends Component {
  state = {
    item: {
      id: this.props.item.id,
      imgSrc: this.props.item.imgSrc,
      title: this.props.item.title,
      price: this.props.item.price,
      description: this.props.item.description,
    },
    isEdit: false,
  };

  handleEdit = () => {
    this.setState({ isEdit: true });
  };

  handleUpdate = (id) => {
    let item0 = this.state.item;
    let item = cleanData(item0);
    axios.put( process.env.REACT_APP_SERVER_BASE_URL + '/items/update', {
      item: item,
    });
    this.setState({ isEdit: false });
    this.setState( {item: item} );
    this.props.changeItemUpdated( item );
  };

  handleInputChange = (event) => {
    this.setState((state, props) => {
      const item = state.item;
      item[event.target.name] = event.target.value;
      return { item };
    });
  };

  render() {
    return this.state.isEdit ? (
      <div className="card" style={{ width: '25%' }}>
        <img
          className="card-img-top"
          style={{
            width: '300px',
            height: '300px',
            objectFit: 'cover',
            objectPosition: '100% 0',
          }}
          src={this.state.item.imgSrc}
          alt={this.state.item.title}
        />
        <input
          name="imgSrc"
          value={this.state.item.imgSrc}
          onChange={this.handleInputChange}
        />
        <br />
        <input
          name="title"
          value={this.state.item.title}
          onChange={this.handleInputChange}
        />
        <br />
        <input
          name="price"
          value={this.state.item.price}
          onChange={this.handleInputChange}
        />
        <br />
        <textarea
          name="description"
          value={this.state.item.description}
          onChange={this.handleInputChange}
        />
        <br />
        <button
          type="button"
          className="btn btn-primary"
          style={{ margin: '3px' }}
          onClick={() => this.handleUpdate(this.props.item.id)}
        >
          Update
        </button>
        <button
          type="button"
          className="btn btn-danger"
          style={{ margin: '3px' }}
          onClick={() => this.props.onDelete(this.props.item.id, this.props.item.title)}
        >
          Delete
        </button>
      </div>
    ) : (
      <div className="col">
        <div className="card">
          <img
            className="card-img-top"
            style={{
              width: '300px',
              height: '300px',
              objectFit: 'cover',
              objectPosition: '100% 0',
            }}
            src={this.state.item.imgSrc}
            alt={this.state.item.title}
          />
          <div className="card-body">
            <h5 className="card-title">{this.state.item.title}</h5>
            <p>
              <strong>{this.state.item.price} USD</strong>
            </p>
            <p className="card-text">
              {ReactHtmlParser(this.state.item.description)}
            </p>
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => this.handleEdit()}
            >
              Edit
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemView;
