import React from 'react';
import ItemAddW from './components/wrapComponents/ItemAdd_w';
import ItemsListW from './components/wrapComponents/ItemsList_w';

const App = () => {
  return (
    <div>
      <h1>Burgers</h1>
      <ItemAddW />
      <ItemsListW />
    </div>
  );
};

export default App;
